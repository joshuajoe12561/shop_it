import 'package:shop_it/models/Rating.dart';

class Product{
 final int id;
 final String title;
 final num price;
 final String description;
 final String category;
 final String image;
 final Rating rating;

  Product({this.id, this.title, this.price, this.description, this.category,
      this.image, this.rating});
 factory Product.fromJson(Map<String, dynamic> json) {
   return Product(
     id: json['id'],
     title: json['title'],
     price: json['price'],
     description: json['description'],
     category: json['category'],
     image: json['image'],
     rating: Rating.fromJson(json['rating'])
   );
 }
}