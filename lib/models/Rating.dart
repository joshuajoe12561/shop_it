class Rating{
  final num rate;
  final num count;

  Rating({this.rate, this.count});
  factory Rating.fromJson(Map<String, dynamic> json) {
    return Rating(rate: json['rate'],count: json['count']);}
}