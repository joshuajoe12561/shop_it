import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:shop_it/models/Product.dart';
import 'package:shop_it/viewmodel/addToCartVM.dart';

class CartItem extends StatelessWidget {
  const CartItem({Key key,  this.product,this.quantity,this.index}): super(key: key);

  final Product product;
  final int quantity;
  final int index;

  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddToCartVM>(
    init: AddToCartVM(),
      builder:(value)=> Container(
        margin: EdgeInsets.all(10),
        decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(15),
            boxShadow: [
              BoxShadow(
                  color: Colors.blue[200].withOpacity(0.3),
                  offset: Offset(0, 0),
                  blurRadius: 3,
                  spreadRadius: 3)
            ]),
        child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
                Expanded(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Image.network(
                      product.image!=null?product.image : 'https://picsum.photos/250?image=9',
                      height: 100,
                      //fit: BoxFit.cover,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(4.0),
                    child: ListTile(
                      title: Text(
                        product.title,
                        style: TextStyle(fontSize: 15),
                      ),
                      trailing: Text(product.price.toString()),
                      subtitle: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Expanded(
                            child: IconButton(
                                alignment: Alignment.topLeft,
                                icon: Icon(Icons.minimize),
                                onPressed: (){
                                  value.del(index);
                            }),
                          ),
                          Padding(padding: EdgeInsets.only(left: 8.0,right: 8.0),
                            child: Text(quantity.toString(),),
                          ),
                          Expanded(
                            child: IconButton(icon: Icon(Icons.add), onPressed: (){
                              value.add(product);
                            }),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
        ]),
      ),
    );
  }
}
