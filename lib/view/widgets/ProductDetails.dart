import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:shop_it/models/Product.dart';
import 'package:shop_it/viewmodel/addToCartVM.dart';

class ProductDetails extends StatelessWidget {
  const ProductDetails({Key key,this.product}):super(key: key);
  final Product product;
  @override
  Widget build(BuildContext context) {
    return LayoutBuilder(
        builder: (context,constraints)=>GetBuilder<AddToCartVM>(
          init: AddToCartVM(),
          builder: (value) =>Scaffold(
            appBar: AppBar(),
            body: Column(

              children: [
                Align(
                  alignment: Alignment.topLeft,
                  child: Container(
                    margin: const EdgeInsets.only(left: 8.0,top: 8.0),
                    child: Text(
                      product.title,style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 25.0),),
                    width: constraints.maxWidth/2,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child:  ClipRRect(
                              borderRadius: BorderRadius.circular(15),
                              child: product.image != null ?
                              Image.network(
                                product.image,
                                height: 150,
                              ):Text('image'),
                            ),),
                ),


              ],
            ),
          )),
    );
  }
}
