import 'package:flutter/material.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:shop_it/view/widgets/cartItem.dart';
import 'package:shop_it/viewmodel/addToCartVM.dart';

class CartScreen extends StatefulWidget {
  CartScreen({Key key,this.size, }):super(key: key);
  final Size size;
  @override
  _CartScreenState createState() => _CartScreenState();
}

class _CartScreenState extends State<CartScreen> {
  @override
  Widget build(BuildContext context) {
    //var screenSize = widget.size;
    return GetBuilder<AddToCartVM>(
      // specify type as Controller
      init: AddToCartVM(), // intialize with the Controller
      builder: (value) => Scaffold(
      appBar: AppBar(
        title: Text("Cart Items"),
      ),
        body: Column(
              children: [
                Expanded(
                  child: value.lst.keys.length>0?
                  ListView.builder(
                           itemCount: value.lst.keys.length,
                           itemBuilder: (context, index) {
                            return
                                CartItem(
                                  //screenSize: Size(200.0,200.0),
                                  product: value.lst.keys.elementAt(index),
                                  quantity: value.lst[value.lst.keys.elementAt(index)],
                                  index: index,
                                );
                          },
                        ):Center(
                            child: Text('Cart empty',)),
                              ),
                SizedBox(height: 10.0,),
                Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      child: RaisedButton(
                        onPressed: () {},
                        child: Text("Checkout"),
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0,right: 20.0,),
                      child: Text('Ksh '+value.total().toStringAsFixed(2),style: TextStyle(fontWeight: FontWeight.bold),),
                    )
                  ],
                ),
              ]),
      ),
    );
  }
}
