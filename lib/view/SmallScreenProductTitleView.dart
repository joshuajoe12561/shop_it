import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shop_it/models/Product.dart';
import 'package:shop_it/view/productItem.dart';
import 'package:shop_it/view/widgets/ProductDetails.dart';

class SmallScreenProductTitleView extends StatelessWidget {
  const SmallScreenProductTitleView({
    Key key,this.snapshot,
    }):super(key: key);

  final AsyncSnapshot snapshot;
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Row(
      children: [
        Container(
          width: size.width*0.4,
          child: GridView.builder(
              itemCount: snapshot.data.length,
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                childAspectRatio: 0.8,
                crossAxisCount: 1,
              ),
              itemBuilder: (context, index) {
                return  GestureDetector(
                  onTap: (){
                    Get.to(()=>ProductDetails(product: snapshot.data[index],));
                  },
                  child: ProductItem(
                            product:snapshot.data[index],
                  ),
                );
              }
          ),
        ),
        SizedBox(height: 10.0,),
        Expanded(child: Container(
          color: Color.fromARGB(248, 150, 191, 110),))
      ],
    );
  }
}

