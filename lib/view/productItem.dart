import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'package:shop_it/models/Product.dart';
import 'package:shop_it/viewmodel/addToCartVM.dart';

class ProductItem extends StatelessWidget{
  const ProductItem(
  {Key key,@required this. product}):super(key: key);

  final Product product;
  @override
  Widget build(BuildContext context) {
    return GetBuilder<AddToCartVM>(
        init: AddToCartVM(),
        builder: (value) => Container(
          margin: EdgeInsets.all(10),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(15),
              boxShadow: [
                BoxShadow(
                    color: Colors.blue[200].withOpacity(0.3),
                    offset: Offset(0, 0),
                    blurRadius: 3,
                    spreadRadius: 3)
              ]),
        child:Card(
          child: Column(
          children: [
              Expanded(
                child: Stack(
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(15),
                        child: product.image != null ?
                        Image.network(
                            product.image,
                            height: 150,
                            ):Text('image'),
                  ),
                    Align(
                      alignment: Alignment.topRight,
                      child: IconButton(
                        onPressed: (){
                          value.updateFavorite(product);
                        },
                        icon: value.fvrt.contains(product)?Icon(
                          Icons.favorite,
                          color: Colors.red,
                          size: 30.0,):
                        Icon(
                          Icons.favorite_border,
                          size: 30.0,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 8.0,),
              Padding(
              padding: const EdgeInsets.all(4.0),
              child: Column(
                        children: [
                    Text(product.title,textScaleFactor: 0.8, maxLines: 1,),
                    Text(
                          'Ksh: '+product.price.toString(),
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        Row(
                          children: [
                             Expanded(
                               child: Container(
                                  decoration: BoxDecoration(
                                          borderRadius: BorderRadius.circular(20.0),
                                          color: Theme.of(context).accentColor,
                                          shape: BoxShape.rectangle
                                      ),
                                  child:  IconButton(
                                      onPressed: (){
                                        value.add(product);
                                      },
                                      icon: Icon(Icons.add_shopping_cart,size: 36.0,),
                                    ),
                                  )
                             ),

                        ])])
              ),]),
        )
    ));
  }
}
