import 'package:flutter/material.dart';
import 'package:shop_it/models/Product.dart';
import 'package:shop_it/view/productItem.dart';

class LargeScreenProductTitleView extends StatelessWidget {
  const LargeScreenProductTitleView({
    Key key,this.snapshot,
  }):super(key: key);

  final AsyncSnapshot snapshot;
  @override
  Widget build(BuildContext context) {
    return GridView.builder(
        itemCount: snapshot.data.length,
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          childAspectRatio: 0.8,
          crossAxisCount: 4,
        ),
        itemBuilder: (context, index) {
          return  ProductItem(
            product:snapshot.data[index],
          );
        }
    );
  }
}