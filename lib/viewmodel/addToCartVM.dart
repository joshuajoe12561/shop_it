import 'package:get/get_state_manager/src/simple/get_controllers.dart';
import 'package:shop_it/models/Product.dart';

class AddToCartVM extends GetxController {
  Map<Product,int> lst = Map();
  List<Product> fvrt = List();
  total(){
    double total=0;
    lst.forEach((key, value) {
      total+=value*key.price;
    });
    return total;
  }
  add(Product product) {
    if (lst.containsKey(product)){
      int noItems = lst[product];
      noItems+=1;
      lst.update(product, (value) => noItems);
    }else{
      lst.putIfAbsent(product, () => 1);
    }
    update();
  }

  del(int index) {
    if(lst[lst.keys.elementAt(index)]>0){
      lst.update(lst.keys.elementAt(index), (value) =>  value-1);
    }if(lst[lst.keys.elementAt(index)]<=0)
    {
      lst.remove(lst.keys.elementAt(index));
    }
    update();
  }
  updateFavorite(Product product) {
    if (fvrt.contains(product)){
      fvrt.remove(product);
    }else{
      fvrt.add(product);
    }
    update();
  }
}
