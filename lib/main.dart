import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_state_manager/src/simple/get_state.dart';
import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:shop_it/models/Product.dart';
import 'package:shop_it/view/LargeScreenProductTitleView.dart';
import 'package:shop_it/view/SmallScreenProductTitleView.dart';
import 'package:shop_it/view/cart.dart';
import 'package:shop_it/view/productItem.dart';
import 'package:shop_it/view/widgets/cartCounter.dart';
import 'package:shop_it/viewmodel/addToCartVM.dart';
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      title: 'Shop it',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        accentColor: Colors.blueGrey[100],
        primaryColor: Colors.blue[200],
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      defaultTransition: Transition.native,
      home: MyHomePage(title: 'Shop it'));
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
   Future<List<Product>> futureAlbum;

  @override
  void initState() {
    super.initState();
    futureAlbum= fetchAlbum();
    print(futureAlbum);
  }

  @override
  Widget build(BuildContext context) {
    var screenSize = MediaQuery.of(context).size;

    return Scaffold(
      appBar: AppBar(
        actions: [
          InkWell(
            onTap: () {
              Get.to(()=>CartScreen(size: screenSize,));
            },
            child: Padding(
              padding:
              const EdgeInsets.only(left: 0, right: 15, top: 8, bottom: 8),
              child: Stack(
                children: [
                  Align(
                      alignment: Alignment.bottomCenter,
                      child: Icon(Icons.shopping_cart_rounded,
                          color: Colors.blue, size: 25)),
                  Positioned(
                    top: 0,
                    left: 0,
                    right: 0,
                    child: GetBuilder<AddToCartVM>(
                      // specify type as Controller
                      init: AddToCartVM(), // intialize with the Controller
                      builder: (value) => CartCounter(
                        count: value.lst.length.toString() ?? "0",
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],

        title: Text(widget.title),
      ),
      body: LayoutBuilder(
        builder: (context,constraints)=>FutureBuilder<List<Product>>(
          future: futureAlbum,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return constraints.maxWidth<=600?
                SmallScreenProductTitleView(snapshot: snapshot):
                LargeScreenProductTitleView(snapshot:snapshot);
            } else if (snapshot.hasError) {
              return Text('${snapshot.error}');
            }
            // By default, show a loading spinner.
            return const CircularProgressIndicator();
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){},
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), 
    );
  }
}
Future<List<Product>> fetchAlbum() async {
  final response = await http
      .get(Uri.parse('https://fakestoreapi.com/products'));
List<Product> productlist = [];
  if (response.statusCode == 200) {
    // If the server did return a 200 OK response,
    // then parse the JSON.
    for(var i in jsonDecode(response.body)){
      productlist.add(Product.fromJson(i));
    }

    return productlist;
  } else {
    // If the server did not return a 200 OK response,
    // then throw an exception.
    throw Exception('Failed to load album');
  }
}
